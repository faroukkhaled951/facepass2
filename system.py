from __future__ import print_function
from typing import Container, Text
from cv2 import data
import requests
import json
import cv2
import numpy as np


content_type="image/jpeg"
headers={'content-type ': content_type}

image=cv2.imread('jack.jpeg')

img_encoded = cv2.imencode('.jpg',image)[1]

file = {'file': ('jack.jpg', img_encoded.tobytes(), 'image/jpeg', {'Expires': '0'})}
data = {"id" : "2345AB"}
response = requests.post("http://127.0.0.1:5000/generate_images", files=file, data=data)
