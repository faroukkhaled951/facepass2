from flask import Flask , request , Response , jsonify
import json 
import numpy as np
import cv2
from werkzeug.utils import send_file
from fake import GenerateImages
import os , io , sys
import base64
import os.path
from PIL import Image
app=Flask(__name__)

#####################
app.config["IMAGES_UPLOADS"] = "/home/farouk/Desktop/farouk_khaled/Inoolabs/gitlab/task_flask/static/SavedData"
######### please change this configration to what you want




#input : the folder where we saved the generated image and want to upload images from and send back to main system
#output : dictioary of key value pair name:the name of the image  value:encoded image to string
def load_images_from_folder(folder):
    '''
        This is function is to encode all images in folder and put them in dictioary to send them back to the main system
    '''
    images_dictioary={}
    for filename in os.listdir(folder):
        img=cv2.imread(os.path.join(folder , filename))
        image_encoded=cv2.imencode('.jpg' , img)[1]
        if img is not None:
            images_dictioary.update({f'{filename}' : f"{image_encoded.tobytes()}"})
    return images_dictioary


@app.route('/generate_images' , methods=['GET' , 'POST'])
def connect():
    if request.method=="POST":
        imag = request.files['file']
        #nparr=np.fromstring( r, np.uint8)
        
        name_only=os.path.splitext(imag.filename)[0]
        path1=os.path.join(app.config["IMAGES_UPLOADS"],name_only)
        try:
            os.mkdir(path1)
        except OSError as err:
            images_dict=load_images_from_folder(os.path.join(app.config["IMAGES_UPLOADS"],name_only))
            return jsonify(images_dict)

        img = cv2.imdecode(np.frombuffer(imag.read(), np.uint8), cv2.IMREAD_UNCHANGED)
        cv2.imwrite(os.path.join(path1, imag.filename),img)
        image_encoded= cv2.imencode('.jpg', img)
        image_encoded= image_encoded[1]
        GenerateImages(image_encoded ,path1,name_only)

        images_dict=load_images_from_folder(os.path.join(app.config["IMAGES_UPLOADS"],name_only) )

        return jsonify(images_dict)
    else:
        return {"msg": " the request must be post request"}
        #no we need to open folder where the images saved and encode it then send it 
       

if __name__ == "__main__":
    app.run(debug=True)


